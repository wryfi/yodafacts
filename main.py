import json
import logging
import re

from suds.client import Client
import requests

YODA_WSDL = 'http://www.yodaspeak.co.uk/webservice/yodatalk.php?wsdl'
FACT_JSON = 'https://uselessfacts.jsph.pl/random.json?language=en'

log = logging.getLogger(__name__)


def yodafact(request):
    """
    returns a random fact in yoda speak
    """
    factyoda = yoda(fact())
    output = json.dumps({'fact': factyoda})
    return output, 200, {'Content-Type': 'application/json; charset=utf-8'}


def fact():
    """
    returns a random fact from the uselessfacts api
    """
    try:
        response = requests.get(FACT_JSON)
        return response.json()['text']
    except Exception as ex:
        log.error(f'error getting fact: {ex}')
        return 'This is not a random fact.'


def yoda(text):
    """
    sends text to the yodaspeak api and returns yoda speak as text
    """
    yoda = Client(YODA_WSDL)
    try:
        yodaspeak = yoda.service.yodaTalk(text)
        yodaspeak = re.sub(r'\s+\.\s+', '', yodaspeak)
        yodaspeak = yodaspeak.strip()
        return yodaspeak
    except Exception as ex:
        log.error(f'error speaking yoda: {ex}')
        return text
